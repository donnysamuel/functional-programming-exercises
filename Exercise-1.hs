maxNum = maximum [4,5]
maxNum2 a b c = maximum [a,b,c]
maxOf3 a b c = max a (max b  c)

repeat5 = take 5 (repeat 5)

repeatLOL  = take 20 (cycle "LOL ")

oddEven xs = [if (x `mod` 2 == 1) then "odd" else "even" | x <- xs]

zipNumWithValue = zip [1,2,3] ["one", "two", "three"]
zip1 xs ys = zip xs ys

-- -> is read as has type of
sum3 :: Int -> Int -> Int -> Int
sum3 a b c = a + b + c

factorial :: Integer -> Integer
factorial n = product [1 .. n]

bmiTell :: (RealFloat a) => a -> a -> String  
bmiTell weight height  
    | bmi <= skinny = "You're underweight, you emo, you!"  
    | bmi <= normal = "You're supposedly normal. Pffft, I bet you're ugly!"  
    | bmi <= fat    = "You're fat! Lose some weight, fatty!"  
    | otherwise     = "You're a whale, congratulations!"  
    where bmi = weight / height ^ 2  
          skinny = 18.5  
          normal = 25.0  
          fat = 30.0  

--  divideByTen 200 = (/10) 200
divideByTen :: (Floating a) => a -> a  
divideByTen = (/10)  

coba1 xs ys = [x+y | x <- xs, y <- ys] 
coba2 xs ys = concat (map (\y -> concat (map (\x -> [x + y]) xs) ) ys)

reverseList xs = reverse xs

length1 xs = length xs

removeNonUpperCase :: String -> String
removeNonUpperCase st = [c | c <- st, c `elem` ['A' .. 'Z']]


-- Lazy Evaluation
x = [ (x,x) | (x,y)<-[ ([1,2],[3,4]),([5,6],[7,8])]]

divisor n = [x | x <- [1 ..], n `mod` x == 0]

-- quicksort :: [] -> []
-- quicksort (x:xs) = quicksort[y | y <- xs, y <= x] + [x] + quicksort[y | y <- xs, y > x]

-- perm [] = [[]]
-- perm ls = [x : sisa | x <- ls, sisa <- perm (ls \\ [x])]

-- Higher Order Function
plus2 xs = map (+1) (map (+1) xs)


data Expr = C Float 
            | Expr :+ Expr 
            | Expr :- Expr 
            | Expr :* Expr 
            | Expr :/ Expr
            | V String 
            | Let String Expr Expr
            deriving Show

subst :: String -> Expr -> Expr -> Expr
subst v0 e0 (V v1) = if (v0 == v1) then e0 else (V v1)
subst _ _ (C c) = (C c)
subst v0 e0 (e1 :+ e2) = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2) = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2) = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2) = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2) -- perlu koreksi ???

evaluate :: Expr -> Float
evaluate (C x) = x
evaluate (e1 :+ e2) = evaluate e1 + evaluate e2
evaluate (e1 :- e2) = evaluate e1 - evaluate e2
evaluate (e1 :* e2) = evaluate e1 * evaluate e2
evaluate (e1 :/ e2) = evaluate e1 / evaluate e2
evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
evaluate (V _) = 0.0 -- is this correct?

-- Mencoba bagian yang perlu dikoreksi
latihan1 = evaluate (Let "x" (C 5) (V "x" :+ C 3))


-- Robot
data Direction = North | East | South | West deriving (Eq,Show,Enum)

right :: Direction -> Direction
right d = toEnum (succ (fromEnum d) `mod` 4)

-- Add energy, supaya bisa ditambah atau dikurangkan
data RobotState = RobotState { 
            position :: Position, 
            facing :: Direction, 
            pen :: Bool, 
            color :: Color, 
            treasure :: [Position], 
            pocket :: Int,
            energy :: Int
    } deriving Show

newtype Robot a = Robot (RobotState -> Grid -> Window -> IO (RobotState, a))
turnRight :: Robot ()
turnRight = updateState (\s -> s {facing = right (facing s)})

moven :: Int -> Robot ()
moven n = mapM_ (const move) [1..n]


-- Merge Sort
merge [] kanan  = kanan
merge kiri []   = kiri
merge kiri@(x:xs) kanan@(y:ys) | x < y = x: merge xs kanan
                               | otherwise = y: merge kiri ys

mergeSort [] = []
mergeSort [x] = [x]
mergeSort xs = merge (mergeSort kiri) (mergeSort kanan)
  where kiri = take ((length xs) div 2) xs
        kanan = drop ((length xs) div 2) xs
